package com.nbody.core.game;

import javax.microedition.khronos.opengles.GL10;

import com.nbody.core.game.NBodyGame.Event;
import com.nbody.core.geom.Vector2;
import com.nbody.core.graph.Grid;
import com.nbody.core.graph.Square;

/**
 * Indicator of the particle size when shooting.
 * 
 * @author Toni Sagrista
 * 
 */
public class ParticleSizeIndicator {
	private static final float maxSize = 1.3f;
	private static final float minSize = .3f;

	private static float x, y;
	private static float squareSize = 75f;

	/**
	 * Size goes from .3 to 1.3
	 */
	private float size;
	private float ratio;
	private boolean active = false;

	private Body sizeIndicator;
	private Square frame;

	public ParticleSizeIndicator(int width, int height) {
		super();
		x = width / 2 - 40f;
		y = -height / 2 + 75f;
		sizeIndicator = new Body(new Vector2(x, y), new Vector2(0f, 0f), 1, 1);
		frame = new Square(1f, squareSize);
		reset();
	}

	public void reset() {
		size = minSize;
		ratio = 0.005f;
	}

	public void update(float dt) {
		dt *= .2f;
		if (NBodyGame.currentEvent == Event.SHOOT && sizeIndicator != null) {
			if (!active) {
				active = true;
				reset();
			}
			size += ratio * dt;
			if (size < minSize) {
				ratio = -ratio;
				size = minSize;
			}
			if (size > maxSize) {
				ratio = -ratio;
				size = maxSize;
			}
		} else {
			active = false;
		}
	}

	public void draw(GL10 gl, Vector2 camera, float zoom) {
		if ( active) {
			// Draw particle
			Grid.beginDrawing(gl, true, false);

			gl.glPushMatrix();

			float dx = -20f, dy = 70;

			gl.glTranslatef(camera.x + dx / zoom, camera.y + dy / zoom, 0f);
			gl.glScalef(1f / zoom, 1f / zoom, 1f);

			sizeIndicator.draw(gl, new float[] { 1f, 1f, 1f, 1f }, size);

			gl.glPopMatrix();

			Grid.endDrawing(gl, true, false);

			// Draw frame
			gl.glPushMatrix();

			gl.glScalef(1f / zoom, 1f / zoom, 1f);
			gl.glTranslatef(x + dx + camera.x * zoom, y + dy + camera.y * zoom, 0f);

			gl.glColor4f(1f, 1f, 1f, 1f);
			gl.glLineWidth(1f);
			frame.draw(gl);

			gl.glPopMatrix();
		}
	}

	private float getNormalizedSizeValue() {
		return size - minSize;
	}

	/**
	 * Returns the mass value
	 * 
	 * @return
	 */
	public float getMassValue() {
		return Body.minMass + (Body.maxMass - Body.minMass) * ((size - minSize) / (maxSize - minSize));
	}
}

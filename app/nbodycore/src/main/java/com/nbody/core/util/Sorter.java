package com.nbody.core.util;

import java.util.Comparator;

/**
 * Sorter abstract class.
 * 
 * @author Toni Sagrista
 * 
 * @param <Type>
 */
public abstract class Sorter<Type> {
	public abstract void sort(Type[] array, int count, Comparator<Type> comparator);
}

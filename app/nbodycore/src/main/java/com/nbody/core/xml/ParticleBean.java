package com.nbody.core.xml;

/**
 * Bean carrying information of a particle.
 * 
 * @author Toni Sagrista
 * 
 */
public class ParticleBean {
	public ParticleBean() {
		super();
	}

	// Is bh
	public boolean bh;

	// Star type
	public String starType;

	// Physical params
	public float radius;
	public float mass;
	public int color;

	// Position
	public float x;
	public float y;

	// Velocity
	public float vx;
	public float vy;

}

package com.nbody.core.geom;

/**
 * Represents a finite segment.
 * 
 * @author Toni Sagrista
 * 
 */
public class Segment2 {
	public Vector2 p1;
	public Vector2 p2;

	public Segment2(Vector2 p1, Vector2 p2) {
		super();
		this.p1 = p1;
		this.p2 = p2;
	}

	public Segment2(float x1, float y1, float x2, float y2) {
		super();
		p1 = new Vector2(x1, y1);
		p2 = new Vector2(x2, y2);
	}

	@Override
	public String toString() {
		return p1.toString() + " - " + p2.toString();
	}

}

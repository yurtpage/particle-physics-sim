package com.nbody.core.game;

import javax.microedition.khronos.opengles.GL10;

import com.nbody.core.geom.Vector2;
import com.nbody.core.util.Constants;
import com.nbody.core.util.FixedSizeArray;

/**
 * A force calculator, which works out the forces acting on every particle at each time step.
 * 
 * @author Toni Sagrista
 * 
 */
public abstract class IForceCalculator {

	public abstract void initialize(float inix, float iniy, float width, float height, boolean displayGrid);

	/**
	 * This method updates the forces exerted on the bodies in the incoming
	 * list.
	 * 
	 * @param bodies
	 */
	public abstract void calculateForces(FixedSizeArray<Body> bodies);

	public int getVectorsUsed() {
		return 0;
	}

	public Vector2 getRepulsionForce(Body body) {
		NBodyGame.aux.set(body.pos);
		NBodyGame.aux.sub(NBodyGame.repPos).normalize();
		// Get distances
		float d = NBodyGame.repPos.distance(body.pos);

		// Calculate force
		float f = (float) (body.mass * Constants.RFmassTimesG / Math.pow(Math.pow(d, 2) + /* eps2 */8, 1.2));

		NBodyGame.aux.scale(f);

		return NBodyGame.aux;
	}

	public void draw(GL10 gl) {
	}

	public void drawMeshPoints(GL10 gl) {
	}
}

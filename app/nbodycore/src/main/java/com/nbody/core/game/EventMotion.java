package com.nbody.core.game;

import javax.microedition.khronos.opengles.GL10;

import com.nbody.core.geom.Vector2;
import com.nbody.core.graph.Line;
import com.nbody.core.graph.Renderable;

/**
 * Represents the line displayed when a particle is shot into the scene.
 * 
 * @author Toni Sagrista
 * 
 */
public class EventMotion extends Renderable {

	float r = 1f, g = 1f, b = 1f, a = 1f;

	public EventMotion() {
		super();
	}

	public EventMotion(Vector2 pos, Vector2 vel, float r, float g, float b, float a) {
		super(pos, vel);
		this.r = r;
		this.g = g;
		this.b = b;
		this.a = a;
	}

	/**
	 * We take pos as initial position and vel as final position
	 * 
	 * @param pos
	 * @param vel
	 */

	public EventMotion(Vector2 pos, Vector2 vel) {
		super(pos, vel);
	}

	public Vector2 getTini() {
		return pos;
	}

	public void setTini(Vector2 tini) {
		this.pos = tini;
	}

	public void setTini(float x, float y) {
		this.pos.x = x;
		this.pos.y = y;
	}

	public Vector2 getTcurrent() {
		return vel;
	}

	public void setTcurrent(Vector2 tcurrent) {
		this.vel = tcurrent;
	}

	public void setTcurrent(float x, float y) {
		this.vel.x = x;
		this.vel.y = y;
	}

	@Override
	public void draw(GL10 gl) {
		if (pos != null && vel != null) {
			Line l = new Line(pos, vel, 4f);
			gl.glPushMatrix();
			gl.glLoadIdentity();
			gl.glColor4f(r, g, b, a);
			l.draw(gl);
			gl.glPopMatrix();
		}

	}

	private Vector2 coordinateConvert(Vector2 in) {

		float x = (in.x / NBodyGame.w) * 2 - 1;
		float y = (in.y / NBodyGame.h) * 2 - 1;

		return new Vector2(x, y);
	}

	public void reset() {
		this.pos.set(0f, 0f);
		this.vel.set(0f, 0f);
	}
}

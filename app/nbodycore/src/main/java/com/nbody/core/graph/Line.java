package com.nbody.core.graph;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;

import javax.microedition.khronos.opengles.GL10;

import com.nbody.core.geom.Vector2;

/**
 * A high-level line that can be drawn in OpenGL.
 * 
 * @author Toni Sagrista
 * 
 */
public class Line {
	// Our vertices.
	private float[] vertices;
	private float width = -1f;

	// Our vertex buffer.
	private FloatBuffer vertexBuffer;

	public Line(Vector2 ini, Vector2 end) {
		super();
		vertices = new float[6];
		this.set(ini, end);
	}

	public Line(float x1, float y1, float x2, float y2) {
		super();
		vertices = new float[6];
		this.set(x1, y1, x2, y2);
	}

	public Line(Vector2 ini, Vector2 end, float width) {
		this(ini, end);
		this.width = width;
	}

	public Line(float x1, float y1, float x2, float y2, float width) {
		this(x1, y1, x2, y2);
		this.width = width;
	}

	public Line() {
		super();
		vertices = new float[6];
		ByteBuffer vbb = ByteBuffer.allocateDirect(6 * 4);
		vbb.order(ByteOrder.nativeOrder());
		vertexBuffer = vbb.asFloatBuffer();
	}

	public Line(float width) {
		this();
		this.width = width;
	}

	public void set(Vector2 ini, Vector2 end) {
		// a float is 4 bytes, therefore we multiply the number if
		// vertices with 4.
		ByteBuffer vbb = ByteBuffer.allocateDirect(6 * 4);
		vbb.order(ByteOrder.nativeOrder());
		vertexBuffer = vbb.asFloatBuffer();
		updateBounds(ini, end);
	}

	public void updateBounds(Vector2 ini, Vector2 end) {
		if (vertices != null && ini != null && end != null) {
			vertices[0] = ini.x;
			vertices[1] = ini.y;
			vertices[2] = 0.0f;

			vertices[3] = end.x;
			vertices[4] = end.y;
			vertices[5] = 0.0f;

			vertexBuffer.clear();
			vertexBuffer.put(vertices);
			vertexBuffer.position(0);
		}
	}

	public void set(float x1, float y1, float x2, float y2) {
		// a float is 4 bytes, therefore we multiply the number if
		// vertices with 4.
		ByteBuffer vbb = ByteBuffer.allocateDirect(6 * 4);
		vbb.order(ByteOrder.nativeOrder());
		vertexBuffer = vbb.asFloatBuffer();
		updateBounds(x1, y1, x2, y2);
	}

	public void updateBounds(float x1, float y1, float x2, float y2) {
		if (vertices != null) {
			vertices[0] = x1;
			vertices[1] = y1;
			vertices[2] = 0.0f;

			vertices[3] = x2;
			vertices[4] = y2;
			vertices[5] = 0.0f;

			vertexBuffer.clear();
			vertexBuffer.put(vertices);
			vertexBuffer.position(0);
		}
	}

	public void setWidth(float width) {
		this.width = width;
	}

	/**
	 * This function draws our line on screen.
	 * 
	 * @param gl
	 */
	public void draw(GL10 gl) {

		// Enabled the vertices buffer for writing and to be used during
		// rendering.
		gl.glEnableClientState(GL10.GL_VERTEX_ARRAY);// OpenGL docs.
		// Specifies the location and data format of an array of vertex
		// coordinates to use when rendering.
		gl.glVertexPointer(3, GL10.GL_FLOAT, 0, // OpenGL docs
				vertexBuffer);
		if (width > 0)
			gl.glLineWidth(width);
		// gl.glEnable(GL10.GL_LINE_SMOOTH);
		gl.glDrawArrays(GL10.GL_LINES, 0, vertices.length / 3);
		// gl.glDisable(GL10.GL_LINE_SMOOTH);

		// Disable the vertices buffer.
		gl.glDisableClientState(GL10.GL_VERTEX_ARRAY); // OpenGL docs
	}

	@Override
	public String toString() {
		String s = "";
		int len = vertices.length;
		for (int i = 0; i < len; i++) {
			s += vertices[i];
			if (i < len - 1)
				s += ", ";
		}
		return "[" + s + "]";
	}

	public void setValues(float x1, float y1, float x2, float y2, float width) {
		set(x1, y1, x2, y2);
		this.width = width;
	}

	public void setIniX(float x) {
		vertices[0] = x;
	}

	public float getIniX() {
		return vertices[0];
	}

	public void setIniY(float y) {
		vertices[1] = y;
	}

	public float getIniY() {
		return vertices[1];
	}

	public void setEndX(float x) {
		vertices[3] = x;
	}

	public float getEndX() {
		return vertices[3];
	}

	public void setEndY(float y) {
		vertices[4] = y;
	}

	public float getEndY() {
		return vertices[4];
	}

}

package com.nbody.core.xml;

/**
 * Bean carrying information of a wall.
 * 
 * @author Toni Sagrista
 * 
 */
public class WallBean {

	public WallBean() {
		super();
	}

	public float x1, x2;
	public float y1, y2;

	public int color;
}

package com.nbody.core.game;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.util.LinkedList;
import java.util.Queue;

import javax.microedition.khronos.opengles.GL10;

import com.nbody.core.geom.Vector2;
import com.nbody.core.graph.GLSprite;
import com.nbody.core.graph.Texture;
import com.nbody.core.graph.TextureHandler;
import com.nbody.core.util.VectorPool;
import com.nbody.core.xml.ParticleBean;

/**
 * A body in the simulation. Holds all its physical parameters and contains the update and draw methods.
 * 
 * @author Toni Sagrista
 * 
 */
public class Body extends GLSprite implements Comparable<Body> {

	public enum StarType {
		STAR, ANTISTAR, BLACKHOLE, STATIC
	}

    private static final float alphaInc = .02f;

	// Mass and radius boundaries
	public static final float rmin = .2f;
	public static final float rbh = 1f;
	public static final float rmax = 2.2f;

	public static final float maxMass = 1E11f;
	public static final float minMass = 3E9f;

	// To make calculations
	private static final Vector2 aux = new Vector2();

	public boolean treated = false;

	int cycle = 1;
	// Keeps velocity vector length
	float vellength;

	/**
	 * 0-white
	 * 1-yellow
	 * 2-red
	 * 3-blue
	 * 4-all
	 */
	public int color;
	float[] c;

	// Collision
	Queue<Body> collide = new LinkedList<Body>();
	float[] tail;
	// Tail pointer
	int tend;

	// Our vertex buffer.
	private static FloatBuffer vertexBuffer;

	static {
		ByteBuffer vbb = ByteBuffer.allocateDirect(6 * 4);
		vbb.order(ByteOrder.nativeOrder());
		vertexBuffer = vbb.asFloatBuffer();
	}
	float lastx, lasty;
	int tailSize = 0;
	// Vector to test whether to add point to trail or not
	Vector2 test;
	StarType starType = StarType.STAR;

	public float radius;
	public float mass; // in Kg
	public Vector2 force; // external force

	/**
	 * Creates a new STAR with the given position, velocity, mass and color
	 * 
	 * @param pos
	 * @param vel
	 * @param mass
	 * @param color
	 */
	public Body(Vector2 pos, Vector2 vel, float mass, int color) {
		this(pos, vel, mass, color, StarType.STAR);
	}

	/**
	 * Creates a new body with the given position, velocity, mass, color
	 * and of the given type (STAR, BLACKHOLE, ANTISTAR)
	 * 
	 * @param pos
	 * @param vel
	 * @param mass
	 * @param color
	 * @param starType
	 */
	public Body(Vector2 pos, Vector2 vel, float mass, int color, StarType starType) {
		this(pos, vel, mass, (starType != StarType.BLACKHOLE ? (computeRadiusFromMass(mass)) : rbh), color);

		this.starType = starType;
		mTexture = getStarTexture(starType);
	}

	/**
	 * Computes the radius of a particle given its mass
	 * 
	 * @param mass
	 * @return
	 */
	private static float computeRadiusFromMass(float mass) {
		// Could use LinearInterpolator, but this is just quicker
		return rmin + (rmax - rmin) * ((mass - minMass) / (maxMass - minMass));
	}

	/**
	 * Creates a new body with the given position, velocity, mass, color
	 * and of the given type (STAR, BLACKHOLE, ANTISTAR)
	 * 
	 * @param pos
	 * @param vel
	 * @param mass
	 * @param radius
	 * @param color
	 * @param starType
	 */
	public Body(Vector2 pos, Vector2 vel, float mass, float radius, int color, StarType starType) {
		this(pos, vel, mass, radius, color);

		this.starType = starType;
		mTexture = getStarTexture(starType);
	}

	public Body(Vector2 pos, Vector2 vel, float mass, float radius, int color) {
		super(pos, vel);
		this.mass = mass;
		this.radius = radius;
		this.color = color;

		this.force = VectorPool.pool.allocate();
		this.test = VectorPool.pool.allocate();

		tail = new float[NBodyGame.ntail];
		tend = -1;

		updateColor();
	}

	/**
	 * Gets the distance from one body to the other
	 * 
	 * @param body
	 * @return
	 */
	public float distance(Body body) {
		return body.pos.distance(pos);
	}

	public Vector2 vector(Body body) {
		return body.pos.copy().sub(pos);
	}

	public void setPosition(float x, float y) {
		this.pos.set(x, y);
	}

	/**
	 * Updates the mass in a mergers scenario.
	 * 
	 * @param mass
	 *            The new mass of the particle
	 * @param radius
	 *            The radius of the particle we are merging with.
	 */
	public void setMass(float mass, float radius) {
		// This for volume float newr = FloatMath.sqrt(this.radius * this.radius + radius * radius);
		float newr = (float) Math.pow(Math.pow(this.radius, 3) + Math.pow(radius, 3), 1f / 3f);
		this.radius = Math.min(newr, rmax);
		this.mass = mass;
	}

	public void addCollision(Body b) {
		collide.add(b);
	}

	public boolean hasCollision() {
		return !collide.isEmpty();
	}

	public Body nextCollide() {
		return collide.poll();
	}

	public void addToTail(float x, float y) {
		tend = updateIndex(tend, tail.length);
		tail[tend] = x;

		tend = updateIndex(tend, tail.length);
		tail[tend] = y;

		if (tailSize + 2 <= tail.length)
			tailSize += 2;

	}

	public int updateIndex(int index, int size) {
		if (index >= size - 1)
			return 0;
		else
			return index + 1;
	}

	public void updatePosition(float x, float y) {
		NBodyGame game = NBodyGame.singleton();
		boolean bounce = false;
		int i = 0;
		// Check walls
		while (!bounce && i < game.walls.mCount) {
			Wall wall = game.walls.get(i);
			aux.set(x - this.pos.x, y - this.pos.y);
			aux.add(this.pos);
			// If we intersect by new position OR
			// our final position distance to the wall is smaller than the radius THEN
			// we got collision
			float distSeg = -1f;
			if (wall.intersects(this.pos.x, this.pos.y, aux.x, aux.y)
					|| (distSeg = wall.getDistanceSegment(aux.x, aux.y)) <= this.getEffectiveRadius()) {
				if (distSeg >= 0
						&& (distSeg == wall.getDistPoint1(aux.x, aux.y) || distSeg == wall.getDistPoint2(aux.x, aux.y))) {
					Wall.ptSegVector(wall.normal2, wall.pos.x, wall.pos.y, wall.vel.x, wall.vel.y, aux.x, aux.y);
					NBodyGame.getMirrorVector(wall.normal2, this.vel, vellength * 0.98f);
				} else {
					NBodyGame.getMirrorVector(wall.normal, this.vel, vellength * 0.98f);
				}
				bounce = true;
			}
			i++;
		}
		if (!bounce) {
			this.pos.x = x;
			this.pos.y = y;
			if (NBodyGame.showTrail) {
				if (tend == -1)
					addToTail(x, y);
				else {
					test.set(tail[tend - 1], tail[tend]);
					float dist = pos.distance(test);
					if (dist >= 10f || vellength == 0)
						addToTail(x, y);
				}
			}
		}
	}

	public boolean collision(float x, float y) {
		return between(x, pos.x - radius, pos.x + radius) && between(y, pos.y - radius, pos.y + radius);
	}

	private boolean between(float value, float bottom, float top) {
		return value >= bottom && value <= top;
	}

	@Override
	public void draw(GL10 gl) {
		// Set color!
		super.draw(gl, c, starType != StarType.BLACKHOLE ? radius : 1f);
	}

	public void drawTail(GL10 gl, float scale) {
		// Draw trail
		int pointer = tend + 1;
		if (tailSize < tail.length) {
			pointer = 0;
		} else if (pointer >= tail.length - 1) {
			pointer = 0;
		}

		float alpha = 0f;

		int sizeCount = 2;

		// Enabled the vertices buffer for writing and to be used during
		// rendering.
		gl.glEnableClientState(GL10.GL_VERTEX_ARRAY);// OpenGL docs.
		// Specifies the location and data format of an array of vertex
		// coordinates to use when rendering.
		gl.glLineWidth(scale * 1.6f);
		while (sizeCount < tailSize) {
			float x1 = tail[pointer];
			float y1 = tail[pointer + 1];
			pointer += 2;
			if (pointer >= tail.length - 1)
				pointer = 0;
			float x2 = tail[pointer];
			float y2 = tail[pointer + 1];

			vertexBuffer.clear();
			vertexBuffer.put(new float[] { x1, y1, 0f, x2, y2, 0f });
			vertexBuffer.position(0);

			if (alpha <= .7f)
				alpha += alphaInc;
			gl.glColor4f(c[0], c[1], c[2], alpha);
			gl.glVertexPointer(3, GL10.GL_FLOAT, 0, // OpenGL docs
					vertexBuffer);
			gl.glDrawArrays(GL10.GL_LINES, 0, 2 /* 2 points, (x, y) */);
			sizeCount += 2;

		}
		vertexBuffer.clear();
		vertexBuffer.put(new float[] { pos.x, pos.y, 0f, tail[pointer], tail[pointer + 1], 0f });
		vertexBuffer.position(0);

		gl.glColor4f(c[0], c[1], c[2], alpha + alphaInc);
		gl.glVertexPointer(3, GL10.GL_FLOAT, 0, // OpenGL docs
				vertexBuffer);
		gl.glDrawArrays(GL10.GL_LINES, 0, 2 /* 2 points, (x, y) */);

		// Disable the vertices buffer.
		gl.glDisableClientState(GL10.GL_VERTEX_ARRAY); // OpenGL docs
	}

	public void updateColor() {
		c = ColorHelper.ch.getColor(color);
	}

	public void clearTail() {
		tail = new float[NBodyGame.ntail];
		tend = -1;
		tailSize = 0;
	}

	/**
	 * Called when a Body is to be disposed
	 */
	public void release() {
		VectorPool.pool.release(pos);
		VectorPool.pool.release(vel);
		VectorPool.pool.release(force);
		VectorPool.pool.release(test);
	}

	/**
	 * This method converts the current body into a black hole
	 */
	public void makeBlackHole() {
		this.starType = StarType.BLACKHOLE;
		mTexture = TextureHandler.getBhTexture(NBodyGame.bhSprite);
		this.color = ColorHelper.ch.getBHColor();
		this.c = ColorHelper.ch.getColor(color);
		mGrid = null;
	}

	/**
	 * This makes this star static
	 */
	public void makeStatic() {
		this.starType = StarType.STATIC;
		mTexture = TextureHandler.getStaticStarTexture(NBodyGame.starSprite);
		mGrid = null;
	}

	public float getEffectiveRadius() {
		// The star?34:24 is the amount of pixels that are unused or unfilled in the image that must be
		// a power of two (usually 64x64)
		return (starType != StarType.BLACKHOLE ? radius : 1f) * (mTexture.mWidth - getUnusedPixels()) / 2;
	}

	public float getInverseRadius(float newr) {
		return 2 * newr / mTexture.mWidth;
	}

	private Texture getStarTexture(StarType starType) {
		switch (starType) {
			case STAR:
				return TextureHandler.getStarTexture(NBodyGame.starSprite);
			case ANTISTAR:
				return TextureHandler.getAntiStarTexture(NBodyGame.starSprite);
			case BLACKHOLE:
				return TextureHandler.getBhTexture(NBodyGame.bhSprite);
			case STATIC:
				return TextureHandler.getStaticStarTexture(NBodyGame.starSprite);
		}
		return null;
	}

	@Override
	public int compareTo(Body another) {
		if (starType == StarType.BLACKHOLE)
			return 1;
		if (another.starType == StarType.BLACKHOLE)
			return -1;
		return 0;
	}

	@Override
	public String toString() {
		return pos != null ? pos.toString() : "[]";
	}

	public boolean isMovable() {
		return this.starType == StarType.STAR || this.starType == StarType.ANTISTAR;
	}

	/**
	 * Exports this body to a bean.
	 * 
	 * @return
	 */
	public ParticleBean exportToBean() {
		ParticleBean pb = new ParticleBean();

		pb.bh = this.starType == StarType.BLACKHOLE;
		pb.starType = this.starType.toString();

		pb.radius = this.radius;
		pb.mass = this.mass;
		pb.color = this.color;

		pb.x = this.pos.x;
		pb.y = this.pos.y;

		pb.vx = this.vel.x;
		pb.vy = this.vel.y;

		return pb;
	}

	/**
	 * Returns the number of unused pixels in an image (horizontally)
	 * 
	 * @return
	 */
	private int getUnusedPixels() {
		if (starType == StarType.BLACKHOLE)
			return 24;
		else
			switch (NBodyGame.starSprite) {
				case 0:
					return 32;
				case 1:
					return 36;
				case 2:
					return 28;
				default:
					return 32;
			}
	}

}

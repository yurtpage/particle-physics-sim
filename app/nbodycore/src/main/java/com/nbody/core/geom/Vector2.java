package com.nbody.core.geom;

/**
 * Simple 2D vector class. Handles basic vector math for 2D vectors.
 */
public final class Vector2 {
	public float x;
	public float y;

	public Vector2() {
		super();
	}

	public Vector2(float xValue, float yValue) {
		set(xValue, yValue);
	}

	public Vector2(Vector2 other) {
		set(other);
	}

	/**
	 * Gets this vector's angle.
	 * 
	 * @return
	 */
	public float angle() {
		return (float) Math.atan2(y, x);
	}

	/**
	 * Gets the angle between this vector and the other vector.
	 * 
	 * @param other
	 * @return
	 */
	public float angle(Vector2 other) {
		return (float) Math.atan2(y, x) - (float) Math.atan2(other.y, other.x);
	}

	public Vector2 add(Vector2 other) {
		x += other.x;
		y += other.y;

		return this;
	}

	public Vector2 add(float otherX, float otherY) {
		x += otherX;
		y += otherY;

		return this;
	}

	public Vector2 subtract(Vector2 other) {
		x -= other.x;
		y -= other.y;

		return this;
	}

	/**
	 * Subtract a vector from this vector
	 * 
	 * @param other
	 *            The vector subtract
	 * @return This vector - useful for chaining operations
	 */
	public Vector2 sub(Vector2 other) {
		x -= other.x;
		y -= other.y;

		return this;
	}

	public Vector2 sub(float otherX, float otherY) {
		return add(-otherX, -otherY);
	}

	public Vector2 multiply(float magnitude) {
		x *= magnitude;
		y *= magnitude;

		return this;
	}

	public Vector2 scale(float magnitude) {
		return multiply(magnitude);
	}

	public Vector2 growVector(float units) {
		final float length = length();
		return this.normalize().scale(length + units);
	}

	public Vector2 multiply(Vector2 other) {
		x *= other.x;
		y *= other.y;

		return this;
	}

	public Vector2 divide(float magnitude) {
		if (magnitude != 0.0f) {
			x /= magnitude;
			y /= magnitude;
		}

		return this;
	}

	public Vector2 set(Vector2 other) {
		x = other.x;
		y = other.y;

		return this;
	}

	public Vector2 set(float xValue, float yValue) {
		x = xValue;
		y = yValue;
		return this;
	}

	public void setX(float xValue) {
		this.x = xValue;
	}

	public void setY(float yValue) {
		this.y = yValue;
	}

	public float dot(Vector2 other) {
		return (x * other.x) + (y * other.y);
	}

	public float length() {
		return (float) Math.sqrt(length2());
	}

	public float length2() {
		return (x * x) + (y * y);
	}

	public float distance2(Vector2 other) {
		float dx = x - other.x;
		float dy = y - other.y;
		return (dx * dx) + (dy * dy);
	}

	public float distance(Vector2 other) {
		return (float) Math.sqrt(distance2(other));
	}

	public Vector2 normalize() {
		final float length = length();

		// TODO: I'm choosing safety over speed here.
		if (length != 0.0f) {
			x /= length;
			y /= length;
		}

		return this;
	}

	/**
	 * Rotates the vector the given radians around the given point
	 * 
	 * @param other
	 */
	public Vector2 rotate(float angle, Vector2 other) {
		double auxx = (x - other.x) * Math.cos(angle) - (y - other.y) * Math.sin(angle);
		double auxy = (y - other.y) * Math.cos(angle) + (x - other.x) * Math.sin(angle);
		x = (float) auxx;
		y = (float) auxy;

		return this;
	}

	/**
	 * The normal of the vector
	 * 
	 * @return A unit vector with the same direction as the vector
	 */
	public final Vector2 getNormal() {
		Vector2 cp = copy();
		cp.normalize();
		return cp;
	}

	/**
	 * A vector perpendicular to this vector.
	 * 
	 * @return a vector perpendicular to this vector
	 */
	public final Vector2 getPerpendicular() {
		return new Vector2(-y, x);
	}

	public final Vector2 makePerpendicular() {
		float aux = this.x;
		this.x = -this.y;
		this.y = aux;
		return this;
	}

	public void zero() {
		set(0.0f, 0.0f);
	}

	public void flipHorizontal(float aboutWidth) {
		x = (aboutWidth - x);
	}

	public void flipVertical(float aboutHeight) {
		y = (aboutHeight - y);
	}

	/*
	 * Return a copy of this vector
	 * 
	 * @return The new instance that copies this vector
	 */
	public Vector2 copy() {
		return new Vector2(x, y);
	}

	@Override
	public String toString() {
		return "[" + x + ", " + y + "]";
	}

}

/*
 * Copyright (C) 2010 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.nbody.core.util;

import android.util.Log;

import com.nbody.core.graph.RegularPolygon;

/**
 * A pool of 2D vectors.
 */
public class PolygonPool extends TObjectPool<RegularPolygon> {

	public static PolygonPool pool;

	public static void initPolygonPool(int size) {
		if (pool != null) {
			// release previously loaded pool
			pool.reset();
			System.gc();
		}
		pool = new PolygonPool(size);
		// pool.logUsed();
	}

	private PolygonPool(int size) {
		super(size);
		fill();
	}

	public PolygonPool() {
		super();
	}

	@Override
	protected void fill() {
		for (int x = 0; x < size; x++) {
			getAvailable().add(new RegularPolygon());
		}
	}

	@Override
	public void release(Object entry) {
		((RegularPolygon) entry).zero();
		super.release(entry);
		// logUsed();
	}

	/** Allocates a vector and assigns the value of the passed source vector to it. */
	public RegularPolygon allocate(RegularPolygon source) {
		RegularPolygon entry = super.allocate();
		entry.set(source);
		// logUsed();
		return entry;
	}

	public RegularPolygon allocate(float incx, float incy, float incz, float inr, int insides) {
		RegularPolygon entry = super.allocate();
		if (entry == null) {
			Log.e("PolygonPool", "PolygonPool full!");
			throw new RuntimeException("Pool is full, can't accomodate more Polygons");
		} else {
			entry.init(incx, incy, incz, inr, insides);
			// logUsed();
			return entry;
		}
	}

	private void logUsed() {
		Log.d("PolygonPool", "Used: " + this.getAllocatedCount() + "\t Remaining: " + this.getFreeCount());
	}

}

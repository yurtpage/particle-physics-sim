package com.nbody.core.game;

import javax.microedition.khronos.opengles.GL10;

import android.util.FloatMath;

import com.nbody.core.geom.Vector2;
import com.nbody.core.graph.Line;
import com.nbody.core.graph.Renderable;
import com.nbody.core.util.VectorPool;
import com.nbody.core.xml.WallBean;

/**
 * Represents a wall in the model of the simulation.
 * 
 * @author Toni Sagrista
 * 
 */
public class Wall extends Renderable {

	Line line;
	Line lineAux;

	// Normal to the wall
	Vector2 normal;
	// Direction along the wall
	Vector2 normal2;

	public Wall() {
		super();
	}

	public Wall(Vector2 ini, Vector2 end) {
		super(ini, end);
		line = new Line(ini, end, 3f);
		lineAux = new Line();
		normal = VectorPool.pool.allocate(vel).sub(ini).getPerpendicular().normalize();
		normal2 = VectorPool.pool.allocate(vel).sub(ini).normalize();
	}

	@Override
	public void draw(GL10 gl) {
		if (pos != null && vel != null) {
			lineAux.set(pos, vel);
			gl.glColor4f(.54f, .352f, .168f, 1f);
			lineAux.draw(gl);
		}
	}

	public boolean intersects(float inix, float iniy, float x, float y) {
		float dx1 = vel.x - pos.x;
		float dx2 = x - inix;
		float dy1 = vel.y - pos.y;
		float dy2 = y - iniy;
		float denom = (dy2 * dx1) - (dx2 * dy1);

		if (denom == 0) {
			return false;
		}

		float ua = (dx2 * (pos.y - iniy)) - (dy2 * (pos.x - inix));
		ua /= denom;
		float ub = (dx1 * (pos.y - iniy)) - (dy1 * (pos.x - inix));
		ub /= denom;

        return (!(ua < 0)) && (!(ua > 1)) && (!(ub < 0)) && (!(ub > 1));

		// float u = ua;

		// float ix = pos.x + (u * (vel.x - pos.x));
		// float iy = pos.y + (u * (vel.y - pos.y));

		// result.set(ix, iy);
    }

	/**
	 * Gets the m in the equation of the line for this wall
	 * y = mx+b, where m is the slope and b is the y-intercept
	 * 
	 * @return
	 */
	public float getM() {
		return (vel.y - pos.y) / (vel.x - pos.x);
	}

	/**
	 * Gets the b in the equation of the line for this wall
	 * y = mx+b, where m is the slope and b is the y-intercept
	 * 
	 * @return
	 */
	public float getB() {
		return pos.y - getM() * pos.x;
	}

	/**
	 * Gets the b in the equation of the line for this wall, using a pre-calculated m
	 * y = mx+b, where m is the slope and b is the y-intercept
	 * 
	 * @return
	 */
	public float getB(float m) {
		return pos.y - m * pos.x;
	}

	/**
	 * Gets the distance from the given point to the wall infinite wall
	 * 
	 * @param point
	 * @return
	 */
	public float getDistance(Vector2 point) {
		return getDistance(point.x, point.y);
	}

	/**
	 * Gets the distance from the point [x, y] to this wall infinite line
	 * 
	 * @param x
	 * @param y
	 * @return
	 */
	public float getDistance(float x, float y) {
		return (float) Math
				.sqrt(((Double) (Math.pow((vel.y - pos.y) * (x - pos.x) - (vel.x - pos.x) * (y - pos.y), 2) / (Math
						.pow(vel.x - pos.x, 2) + Math.pow(vel.y - pos.y, 2)))).floatValue());
	}

	/**
	 * Gets the distance from the given point to this wall
	 * 
	 * @param point
	 * @return
	 */
	public float getDistanceSegment(Vector2 point) {
		return getDistanceSegment(point.x, point.y);
	}

	public float getDistPoint(Vector2 point, float x, float y) {
		float dx = point.x - x;
		float dy = point.y - y;
		return (float) Math.sqrt((dx * dx) + (dy * dy));
	}

	public float getDistPoint1(float x, float y) {
		return getDistPoint(pos, x, y);
	}

	public float getDistPoint2(float x, float y) {
		return getDistPoint(vel, x, y);
	}

	/**
	 * Gets the distance from the point [x, y] to this wall
	 * 
	 * @param x
	 * @param y
	 * @return
	 */
	public float getDistanceSegment(float x, float y) {
		return (float) Math.sqrt(ptSegDistSq(pos.x, pos.y, vel.x, vel.y, x, y));
	}

	/**
	 * Returns the square of the distance from a point to a line segment.
	 * The distance measured is the distance between the specified
	 * point and the closest point between the specified end points.
	 * If the specified point intersects the line segment in between the
	 * end points, this method returns 0.0.
	 * 
	 * @param x1
	 *            the X coordinate of the start point of the
	 *            specified line segment
	 * @param y1
	 *            the Y coordinate of the start point of the
	 *            specified line segment
	 * @param x2
	 *            the X coordinate of the end point of the
	 *            specified line segment
	 * @param y2
	 *            the Y coordinate of the end point of the
	 *            specified line segment
	 * @param px
	 *            the X coordinate of the specified point being
	 *            measured against the specified line segment
	 * @param py
	 *            the Y coordinate of the specified point being
	 *            measured against the specified line segment
	 * @return a double value that is the square of the distance from the
	 *         specified point to the specified line segment.
	 * @since 1.2
	 */
	public static float ptSegDistSq(float x1, float y1, float x2, float y2, float px, float py) {
		// Adjust vectors relative to x1,y1
		// x2,y2 becomes relative vector from x1,y1 to end of segment
		x2 -= x1;
		y2 -= y1;
		// px,py becomes relative vector from x1,y1 to test point
		px -= x1;
		py -= y1;
		float dotprod = px * x2 + py * y2;
		float projlenSq;
		if (dotprod <= 0.0) {
			// px,py is on the side of x1,y1 away from x2,y2
			// distance to segment is length of px,py vector
			// "length of its (clipped) projection" is now 0.0
			projlenSq = 0.0f;
		} else {
			// switch to backwards vectors relative to x2,y2
			// x2,y2 are already the negative of x1,y1=>x2,y2
			// to get px,py to be the negative of px,py=>x2,y2
			// the dot product of two negated vectors is the same
			// as the dot product of the two normal vectors
			px = x2 - px;
			py = y2 - py;
			dotprod = px * x2 + py * y2;
			if (dotprod <= 0.0) {
				// px,py is on the side of x2,y2 away from x1,y1
				// distance to segment is length of (backwards) px,py vector
				// "length of its (clipped) projection" is now 0.0
				projlenSq = 0.0f;
			} else {
				// px,py is between x1,y1 and x2,y2
				// dotprod is the length of the px,py vector
				// projected on the x2,y2=>x1,y1 vector times the
				// length of the x2,y2=>x1,y1 vector
				projlenSq = dotprod * dotprod / (x2 * x2 + y2 * y2);
			}
		}
		// Distance to line is now the length of the relative point
		// vector minus the length of its projection onto the line
		// (which is zero if the projection falls outside the range
		// of the line segment).
		float lenSq = px * px + py * py - projlenSq;
		if (lenSq < 0) {
			lenSq = 0;
		}
		return lenSq;
	}

	public static void ptSegVector(Vector2 v, float x1, float y1, float x2, float y2, float px, float py) {
		// Adjust vectors relative to x1,y1
		// x2,y2 becomes relative vector from x1,y1 to end of segment
		x2 -= x1;
		y2 -= y1;
		// px,py becomes relative vector from x1,y1 to test point
		px -= x1;
		py -= y1;
		float dotprod = px * x2 + py * y2;
		float projlenSq;
		if (dotprod <= 0.0) {
			// px,py is on the side of x1,y1 away from x2,y2
			// distance to segment is length of px,py vector
			// "length of its (clipped) projection" is now 0.0
			projlenSq = 0.0f;
		} else {
			// switch to backwards vectors relative to x2,y2
			// x2,y2 are already the negative of x1,y1=>x2,y2
			// to get px,py to be the negative of px,py=>x2,y2
			// the dot product of two negated vectors is the same
			// as the dot product of the two normal vectors
			px = x2 - px;
			py = y2 - py;
			dotprod = px * x2 + py * y2;
			if (dotprod <= 0.0) {
				// px,py is on the side of x2,y2 away from x1,y1
				// distance to segment is length of (backwards) px,py vector
				// "length of its (clipped) projection" is now 0.0
				projlenSq = 0.0f;
			} else {
				// px,py is between x1,y1 and x2,y2
				// dotprod is the length of the px,py vector
				// projected on the x2,y2=>x1,y1 vector times the
				// length of the x2,y2=>x1,y1 vector
				projlenSq = dotprod * dotprod / (x2 * x2 + y2 * y2);
			}
		}
		// Distance to line is now the length of the relative point
		// vector minus the length of its projection onto the line
		// (which is zero if the projection falls outside the range
		// of the line segment).
		v.set(px, py);
	}

	/**
	 * Exports this wall to a bean
	 * 
	 * @return
	 */
	public WallBean exportToBean() {
		WallBean w = new WallBean();
		w.x1 = this.pos.x;
		w.y1 = this.pos.y;
		w.x2 = this.vel.x;
		w.y2 = this.vel.y;

		return w;
	}

}

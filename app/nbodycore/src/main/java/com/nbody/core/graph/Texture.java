package com.nbody.core.graph;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;

import javax.microedition.khronos.opengles.GL10;
import javax.microedition.khronos.opengles.GL11;
import javax.microedition.khronos.opengles.GL11Ext;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.opengl.GLUtils;
import android.util.Log;

/**
 * A single texture resource.
 * 
 * @author Toni Sagrista
 * 
 */
public class Texture {
	public int mWidth, mHeight;
	public boolean mFromResource = true;
	private int[] mTextures = new int[1];
	private int[] mCropWorkspace = new int[4];

	// The OpenGL ES texture handle to draw.
	public Integer mTextureName;
	// The id of the original resource that mTextureName is based on.
	public Integer mResourceId = null;
	public String mPath = null;

	public Texture(int resourceId) {
		super();
		this.mResourceId = resourceId;
	}

	public Texture(int resourceId, boolean fromResource) {
		super();
		this.mResourceId = resourceId;
		this.mFromResource = fromResource;
	}

	public Texture(String path) {
		super();
		this.mPath = path;
	}

	public boolean isLoaded() {
		return mTextureName != null;
	}

	public void unloadTexture(Context context, GL10 gl) {
		gl.glDeleteTextures(1, new int[] { mTextureName }, 0);
	}

	public Texture loadTexture(Context context, GL10 gl) {
		if (mResourceId != null && mFromResource) // TODO change this shit
			return loadTexture(context, gl, mResourceId);
		else
			return loadTexture(context, gl, mPath);
	}

	/**
	 * Loads a bitmap into OpenGL and sets up the common parameters for
	 * 2D texture maps.
	 */
	public Texture loadTexture(Context context, GL10 gl, int resourceId) {

		BitmapFactory.Options sBitmapOptions = new BitmapFactory.Options();
		// Set our bitmaps to 16-bit, 565 format.
		sBitmapOptions.inPreferredConfig = Bitmap.Config.RGB_565;
		gl.glGenTextures(1, mTextures, 0);

		gl.glBindTexture(GL10.GL_TEXTURE_2D, mTextures[0]);

		gl.glTexParameterf(GL10.GL_TEXTURE_2D, GL10.GL_TEXTURE_MIN_FILTER, GL10.GL_NEAREST);
		gl.glTexParameterf(GL10.GL_TEXTURE_2D, GL10.GL_TEXTURE_MAG_FILTER, GL10.GL_LINEAR);

		gl.glTexParameterf(GL10.GL_TEXTURE_2D, GL10.GL_TEXTURE_WRAP_S, GL10.GL_CLAMP_TO_EDGE);
		gl.glTexParameterf(GL10.GL_TEXTURE_2D, GL10.GL_TEXTURE_WRAP_T, GL10.GL_CLAMP_TO_EDGE);

		gl.glTexEnvf(GL10.GL_TEXTURE_ENV, GL10.GL_TEXTURE_ENV_MODE, GL10.GL_REPLACE);

		InputStream is = context.getResources().openRawResource(resourceId);
		Bitmap bitmap;
		try {
			bitmap = BitmapFactory.decodeStream(is, null, sBitmapOptions);
		} finally {
			try {
				is.close();
			} catch (IOException e) {
				// Ignore.
				Log.e("NBodyGame", e.getMessage());
			}
		}

		GLUtils.texImage2D(GL10.GL_TEXTURE_2D, 0, bitmap, 0);

		mCropWorkspace[0] = 0;
		mCropWorkspace[1] = bitmap.getHeight();
		mCropWorkspace[2] = bitmap.getWidth();
		mCropWorkspace[3] = -bitmap.getHeight();

		mWidth = bitmap.getWidth();
		mHeight = bitmap.getHeight();

		bitmap.recycle();

		((GL11) gl).glTexParameteriv(GL10.GL_TEXTURE_2D, GL11Ext.GL_TEXTURE_CROP_RECT_OES, mCropWorkspace, 0);

		int error = gl.glGetError();
		if (error != GL10.GL_NO_ERROR) {
			Log.e("NBodyGame", "Texture Load GLError: " + error);
		}
		mTextureName = mTextures[0];
		return this;
	}

	/**
	 * Loads a bitmap into OpenGL and sets up the common parameters for
	 * 2D texture maps.
	 */
	public Texture loadTexture(Context context, GL10 gl, String path) {

		BitmapFactory.Options sBitmapOptions = new BitmapFactory.Options();
		// Set our bitmaps to 16-bit, 565 format.
		sBitmapOptions.inPreferredConfig = Bitmap.Config.RGB_565;
		gl.glGenTextures(1, mTextures, 0);

		gl.glBindTexture(GL10.GL_TEXTURE_2D, mTextures[0]);

		gl.glTexParameterf(GL10.GL_TEXTURE_2D, GL10.GL_TEXTURE_MIN_FILTER, GL10.GL_NEAREST);
		gl.glTexParameterf(GL10.GL_TEXTURE_2D, GL10.GL_TEXTURE_MAG_FILTER, GL10.GL_LINEAR);

		gl.glTexParameterf(GL10.GL_TEXTURE_2D, GL10.GL_TEXTURE_WRAP_S, GL10.GL_CLAMP_TO_EDGE);
		gl.glTexParameterf(GL10.GL_TEXTURE_2D, GL10.GL_TEXTURE_WRAP_T, GL10.GL_CLAMP_TO_EDGE);

		gl.glTexEnvf(GL10.GL_TEXTURE_ENV, GL10.GL_TEXTURE_ENV_MODE, GL10.GL_REPLACE);

		mFromResource = false;
		File img = new File(path);

		Bitmap bitmap = null;
		if (img.exists() && img.isFile() && img.canRead()) {
			bitmap = BitmapFactory.decodeFile(img.getAbsolutePath(), sBitmapOptions);
		} else {
			Log.e("NBodyGame", "Texture '" + path + "' does not exist and thus could not be loaded");
			return this;
		}

		GLUtils.texImage2D(GL10.GL_TEXTURE_2D, 0, bitmap, 0);

		mCropWorkspace[0] = 0;
		mCropWorkspace[1] = bitmap.getHeight();
		mCropWorkspace[2] = bitmap.getWidth();
		mCropWorkspace[3] = -bitmap.getHeight();

		mWidth = bitmap.getWidth();
		mHeight = bitmap.getHeight();

		bitmap.recycle();

		((GL11) gl).glTexParameteriv(GL10.GL_TEXTURE_2D, GL11Ext.GL_TEXTURE_CROP_RECT_OES, mCropWorkspace, 0);

		int error = gl.glGetError();
		if (error != GL10.GL_NO_ERROR) {
			Log.e("NBodyGame", "Texture Load GLError: " + error);
		}
		mTextureName = mTextures[0];
		return this;
	}

}

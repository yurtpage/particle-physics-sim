package com.nbody.core.game;

import com.nbody.core.geom.Vector2;

/**
 * Handles the camera panning.
 * 
 * @author Toni Sagrista
 * 
 */
public class Camera extends EventMotion {

	public boolean panning = false;
	public Vector2 difference;
	public Vector2 currentDifference;
	// Auxiliar vector to return on certain occasions
	public Vector2 pan;

	public Camera(float centerx, float centery) {
		super(new Vector2(), new Vector2());
		difference = new Vector2(centerx, centery);
		currentDifference = new Vector2();
		pan = new Vector2();
	}

	public void updateCurrentDifference() {
		currentDifference.set(pos.x - vel.x, pos.y - vel.y);
	}

	public void clearCurrentDifference() {
		currentDifference.set(0f, 0f);
	}

	public void updateDifference() {
		difference.add(currentDifference);
	}

	public Vector2 getPan() {
		if (panning)
			return pan.set(difference).add(currentDifference);
		else
			return difference;
	}

	public Vector2 getPan(float x, float y) {
		if (panning)
			return difference.copy().add(pos.x - x, pos.y - y);
		else
			return difference;
	}

	public Vector2 getPan(float x, float y, float zoom) {
		if (panning)
			return difference.copy().add(pos.x - x, pos.y - y).divide(zoom);
		else
			return difference.copy().divide(zoom);
	}
}

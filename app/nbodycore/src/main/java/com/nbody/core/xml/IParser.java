package com.nbody.core.xml;

import java.io.File;

/**
 * A parser of XML simulation files.
 * 
 * @author Toni Sagrista
 * 
 */
public interface IParser {
	SimulationBean parseXml(File file);

	boolean writeXml(SimulationBean bean, File file);
}

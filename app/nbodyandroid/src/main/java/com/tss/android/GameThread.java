package com.tss.android;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.graphics.Color;
import android.graphics.PorterDuff.Mode;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;

import com.nbody.core.game.NBodyGame;
import com.nbody.core.game.NBodyGame.TouchState;
import com.nbody.core.graph.GameRenderer;
import com.nbody.core.util.Keys;
import com.nbody.core.xml.SimulationBean;

/**
 * The class that controls the game thread
 *
 * @author tsagrista
 */
public class GameThread extends Thread implements OnTouchListener, SensorEventListener, OnSeekBarChangeListener {
    // Game instance
    private NBodyGame mGame;
    private GameRenderer mRenderer;
    private NBodyActivity mActivity;
    /*
     * State-tracking constants
     */
    public static final int STATE_PAUSE = 1;
    public static final int STATE_READY = 2;
    public static final int STATE_RUNNING = 3;
    public static final int STATE_STOP = 4;

    private int canvasWidth;
    private int canvasHeight;
    private long t, dt;

    private TouchState touchState = TouchState.NONE;

    /**
     * Message handler used by thread to post stuff back to the GameView
     */
    private Handler mHandler;
    private Context mContext;

    /**
     * The state of the game. One of READY, RUNNING, PAUSE, LOSE, or WIN
     */
    private int mMode;
    /**
     * Indicate whether the surface has been created & is ready to draw
     */
    private boolean mRun = false;
    /**
     * Handle to the surface manager object we interact with
     */
    private Object mLock;
    private SharedPreferences sp;
    private int lastVelocity;

    public GameThread(Context context, Handler handler, NBodyActivity activity) {
        // get handles to some important objects
        mLock = new Object();
        mHandler = handler;
        mContext = context;
        mActivity = activity;

        SensorManager sm;
        Sensor accel;
        sm = (SensorManager) context.getSystemService(Activity.SENSOR_SERVICE);
        accel = sm.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        sm.registerListener(this, accel, SensorManager.SENSOR_DELAY_NORMAL);

        sp = PreferenceManager.getDefaultSharedPreferences(mContext);

        mGame = NBodyGame.initSingleton(new NBodyGame(mContext));
        lastVelocity = sp.getInt(Keys.KEY_VELOCITY, 50);
        mGame.dtscale = NBodyGame.computeDtscale(lastVelocity);

    }

    public void reload() {
        mGame = NBodyGame.initSingleton(new NBodyGame(mContext));
        doRestart();
    }

    public void reload(SimulationBean sb) {
        mGame = NBodyGame.initSingleton(new NBodyGame(mContext, sb));
        doRestart(sb);
    }

    public void doStart() {
        doRestart();
    }

    /**
     * Restarts the game, setting parameters for the current difficulty.
     */
    public void doRestart() {
        doRestart(null);
    }

    /**
     * Restarts the game, setting parameters for the current difficulty.
     */
    public void doRestart(SimulationBean sb) {
        setState(STATE_PAUSE);

        // Initialize game here!
        NBodyGame.setW(canvasWidth);
        NBodyGame.setH(canvasHeight);
        mGame.init(sb);
        t = System.currentTimeMillis() + 100;

        setState(STATE_RUNNING);

    }

    /**
     * Restores game state from the indicated Bundle. Typically called when
     * the Activity is being restored after having been previously
     * destroyed.
     *
     * @param savedState Bundle containing the game state
     */
    public synchronized void restoreState(Bundle savedState) {
        synchronized (mLock) {
            setState(STATE_PAUSE);
        }
    }

    /**
     * Dump game state to the provided Bundle. Typically called when the
     * Activity is being suspended.
     *
     * @return Bundle with this view's state
     */
    public Bundle saveState(Bundle map) {
        synchronized (mLock) {
            if (map != null) {

            }
        }
        return map;
    }

    @Override
    public void run() {
        while (mRun) {
            try {
                // synchronized (mLock) {
                if (mMode == STATE_RUNNING) {
                    long now = System.currentTimeMillis();
                    // Do nothing if mLastTime is in the future.
                    // This allows the game-start to delay the start of the physics
                    // by 100ms or whatever.
                    dt = (now - t);
                    t = now;

                    mGame.update(dt);
                } else if (mMode == STATE_PAUSE) {
                    sleep(500);
                }
                // No more rendering here
                // }
            } catch (Exception e) {
                Log.e("NBodyGame", e.getMessage());
            } finally {
                // do this in a finally so that if an exception is thrown
                // during the above, we don't leave the Surface in an
                // inconsistent state
            }
        }
    }

    /**
     * Used to signal the thread whether it should be running or not.
     * Passing true allows the thread to run; passing false will shut it
     * down if it's already running. Calling start() after this was most
     * recently called with false will result in an immediate shutdown.
     *
     * @param b true to run, false to shut down
     */
    public void setRunning(boolean b) {
        mRun = b;
    }

    public boolean isStateRunning() {
        return mMode == STATE_RUNNING;
    }

    /**
     * Sets the game mode. That is, whether we are running, paused, in the
     * failure state, in the victory state, etc.
     *
     * @param mode one of the STATE_* constants
     * @see #setState(int, CharSequence)
     */
    public void setState(int mode) {
        setState(mode, null);
    }

    /**
     * Sets the game mode. That is, whether we are running, paused, in the
     * failure state, in the victory state, etc.
     *
     * @param mode    one of the STATE_* constants
     * @param message string to add to screen or null
     */
    public void setState(int mode, CharSequence message) {
        synchronized (mLock) {
            mMode = mode;
        }
    }

    /* Callback invoked when the surface dimensions change. */
    public void setSurfaceSize(int width, int height) {
        // synchronized to make sure these all change atomically
        synchronized (mLock) {
            canvasWidth = width;
            canvasHeight = height;
        }
    }

    /**
     * Pauses the physics update & animation.
     */
    public void pause() {
        // synchronized (mLock) {
        if (mMode == STATE_RUNNING)
            setState(STATE_PAUSE);
        // }
        // Save seekbar
        Editor ed = sp.edit();
        ed.putInt(Keys.KEY_VELOCITY, lastVelocity);
        ed.commit();
    }

    /**
     * Resumes from a pause.
     */
    public void unpause() {
        // Move the real time clock up to now
        if (mMode == STATE_PAUSE) {
            touchT();
            setState(STATE_RUNNING);
        }
    }

    public void destroy() {
        synchronized (mLock) {
            setState(STATE_STOP);
        }
    }

    public void touchT() {
        t = System.currentTimeMillis();
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        switch (v.getId()) {
            case R.id.resetCameraButton:
                final ImageButton mResetCameraButton = (ImageButton) v;
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    mResetCameraButton.setColorFilter(Interface.buttonColor, Mode.MULTIPLY);
                } else if (event.getAction() == MotionEvent.ACTION_UP) {
                    // Reset Camera
                    mGame.initCamera();
                    mRenderer.touchCamera();
                    Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        public void run() {
                            mResetCameraButton.clearColorFilter();
                        }
                    }, 1000);
                }
                break;
            case R.id.panButton:
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    // Pan
                    ImageButton mPanButton = (ImageButton) v;
                    if (touchState != TouchState.PAN) {
                        mPanButton.setColorFilter(Interface.buttonColor, Mode.MULTIPLY);
                        touchState = TouchState.PAN;
                        // Clear others
                        Interface.mShootButton.clearColorFilter();
                        Interface.mRepelButton.clearColorFilter();
                        Interface.mWallButton.clearColorFilter();
                    } else {
                        mPanButton.clearColorFilter();
                        touchState = TouchState.NONE;
                    }
                }
                break;
            case R.id.shootButton:
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    // Shoot
                    if (touchState != TouchState.SHOOT && touchState != TouchState.SHOOT_ANTI
                            && touchState != TouchState.SHOOT_STATIC) {
                        Interface.mShootButton.setColorFilter(Interface.buttonColor, Mode.MULTIPLY);
                        touchState = TouchState.SHOOT;
                        // Clear others
                        Interface.mPanButton.clearColorFilter();
                        Interface.mRepelButton.clearColorFilter();
                        Interface.mWallButton.clearColorFilter();
                    } else if (touchState == TouchState.SHOOT) {
                        // Change to anti only if not PM
                        if (NBodyGame.forceMethod.equals(NBodyGame.ForceMethod.DIRECT)) {
                            Interface.mShootButton.setImageResource(R.drawable.shootanti);
                            Interface.mShootButton.setColorFilter(Interface.buttonColor, Mode.MULTIPLY);
                            touchState = TouchState.SHOOT_ANTI;
                        } else {
                            Interface.mShootButton.setImageResource(R.drawable.shoot);
                            Interface.mShootButton.clearColorFilter();
                            touchState = TouchState.NONE;
                        }
                    } else if (touchState == TouchState.SHOOT_ANTI) {
                        Interface.mShootButton.setImageResource(R.drawable.shootstatic);
                        Interface.mShootButton.setColorFilter(Interface.buttonColor, Mode.MULTIPLY);
                        touchState = TouchState.SHOOT_STATIC;
                    } else if (touchState == TouchState.SHOOT_STATIC) {
                        Interface.mShootButton.setImageResource(R.drawable.shoot);
                        Interface.mShootButton.clearColorFilter();
                        touchState = TouchState.NONE;
                    }
                }
                break;
            case R.id.repelButton:
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    // Repel
                    ImageButton mRepelButton = (ImageButton) v;
                    if (touchState != TouchState.REPEL) {
                        mRepelButton.setColorFilter(Interface.buttonColor, Mode.MULTIPLY);
                        touchState = TouchState.REPEL;
                        // Clear others
                        Interface.mPanButton.clearColorFilter();
                        Interface.mShootButton.clearColorFilter();
                        Interface.mWallButton.clearColorFilter();
                    } else {
                        mRepelButton.clearColorFilter();
                        touchState = TouchState.NONE;
                    }
                }
                break;
            case R.id.wallButton:
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    // Repel
                    ImageButton mWallButton = (ImageButton) v;
                    if (touchState != TouchState.WALL) {
                        mWallButton.setColorFilter(Interface.buttonColor, Mode.MULTIPLY);
                        touchState = TouchState.WALL;
                        // Clear others
                        Interface.mPanButton.clearColorFilter();
                        Interface.mShootButton.clearColorFilter();
                        Interface.mRepelButton.clearColorFilter();
                    } else {
                        mWallButton.clearColorFilter();
                        touchState = TouchState.NONE;
                    }
                }
                break;
            case R.id.accelButton:
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    // Enable/disable accelerometer
                    ImageButton mAccelButton = (ImageButton) v;
                    if (NBodyGame.accelerometer) {
                        // disable!
                        mGame.setAccelerometer(false);
                        mAccelButton.clearColorFilter();
                    } else {
                        // enable;
                        mGame.setAccelerometer(true);
                        mAccelButton.setColorFilter(Interface.buttonColor, Mode.MULTIPLY);
                    }
                }
                break;
            case R.id.tailButton:
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    // Enable/disable trail
                    ImageButton mTailButton = (ImageButton) v;
                    if (NBodyGame.showTrail) {
                        // disable!
                        mGame.setShowTail(false);
                        mTailButton.clearColorFilter();
                        // Clear trails
                        for (int i = 0; i < mGame.bodies.mCount; i++)
                            mGame.bodies.get(i).clearTail();

                    } else {
                        // enable;
                        mGame.setShowTail(true);
                        mTailButton.setColorFilter(Interface.buttonColor, Mode.MULTIPLY);
                    }
                }
                break;
            case R.id.fpsButton:
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    // Enable/disable fps display
                    Button mFpsButton = (Button) v;
                    if (mActivity.isInfoVisible()) {
                        // disable!
                        mActivity.setInfoVisibility(false);
                        mFpsButton.setTextColor(Color.rgb(160, 160, 160));
                    } else {
                        // enable;
                        mActivity.setInfoVisibility(true);
                        mFpsButton.setTextColor(Color.rgb(50, 140, 50));
                    }
                }
                break;
            case R.id.glsurfaceview:
                if (mMode == STATE_RUNNING && mGame != null) {
                    mGame.onTouch(event, touchState);
                }
                break;
        }

        return true;
    }

    @Override
    public void onAccuracyChanged(Sensor arg0, int arg1) {
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        if (mMode == STATE_RUNNING && mGame != null) {
            mGame.onSensorChanged(event);
        }
    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
        float newdt = NBodyGame.computeDtscale(progress);

        mGame.setDtscale(newdt);
        lastVelocity = progress;

    }

    /**
     * Gets the simulation bean of the current simulation
     *
     * @return
     */
    public SimulationBean getSimulationBean() {
        return mGame.exportToSimulationBean();
    }

    public GameRenderer getmRenderer() {
        return mRenderer;
    }

    public void setmRenderer(GameRenderer mRenderer) {
        this.mRenderer = mRenderer;
    }

    @Override
    public void onStartTrackingTouch(SeekBar arg0) {
    }

    @Override
    public void onStopTrackingTouch(SeekBar arg0) {
    }

}

package com.tss.android;

import android.graphics.Color;
import android.widget.Button;
import android.widget.ImageButton;

public class Interface {
    public static ImageButton mPanButton, mRepelButton, mShootButton, mResetCameraButton, mWallButton, mAccelButton, mTailButton;
    public static Button mFpsButton, mMenuButton;
    public static int buttonColor = Color.argb(255, 100, 255, 100);
}
